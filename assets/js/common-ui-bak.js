'use strict';

// S: Dynamic Tab + Content -- jQuery
$(function () {
	// show/hide 처리
	$(document).on('click', '[role="show"]', function (event) {
		event = event || window.event;
		event.preventDefault ? event.preventDefault() : (event.returnValue = false);

		// 선택된 탭 활성화
		$('#' + $(this).attr('aria-controls'))
			.removeAttr('hidden')
			.attr({ tabindex: '0', 'aria-hidden': 'false' })
			.addClass('is-active')
			// 기존 탭 패널 비활성화
			.siblings('.showpanel')
			.attr({ tabindex: '-1', 'aria-hidden': 'true', hidden: '' })
			.removeClass('is-active');
	});

	// let tabFocus = 0;
	$(document).on('click', '[role="tab"]', function (event) {
		event = event || window.event;
		event.preventDefault ? event.preventDefault() : (event.returnValue = false);

		// 선택된 탭 활성화
		tabActivate($(this));
		// 탭 하위에 탭이 존재하는 경우 초기화 처리
		var $thisChildren = $('#' + $(this).attr('aria-controls')).find('[role="tablist"]');
		if ($thisChildren.length) {
			tabActivate($thisChildren.children().eq(0).children('[role="tab"]'));
			$('#' + $(this).attr('aria-controls')).removeAttr('tabindex');
		}
	});

	$(document).on('keydown', '[role="tab"]', function (event) {
		event = event || window.event;
		var keycode = event.keyCode || event.which;
		var tabsLength = $(this).children('li').length;
		if (keycode === 39 || keycode === 37) {
			var firstEl = $(this).closest('[role="tablist"]').children('li').eq(0).children('[role="tab"]');
			var lastEl = $(this)
				.closest('[role="tablist"]')
				.children('li')
				.eq(tabsLength - 1)
				.children('[role="tab"]');
			$(this).parent('li').addClass('');
			if (keycode === 39) {
				$(this).parent('li').next().children('[role="tab"]').focus();
				if (!$(this).parent('li').next().length) {
					firstEl.focus();
				}
			} else if (keycode === 37) {
				$(this).parent('li').prev().children('[role="tab"]').focus();
				if (!$(this).parent('li').prev().length) {
					lastEl.focus();
				}
			}
		}
	});

	// Collapse 처리
	// 초기화
	$('.is-collapsible').each(function () {
		if ($(this).hasClass('is-active')) {
			$(this).attr({ 'aria-expanded': 'true' });
		} else {
			$(this).attr({ 'aria-expanded': 'false' }).css({ display: 'none' });
		}
	});
	$('[data-action="collapse"]').each(function () {
		if ($($(this).attr('href')).hasClass('is-active')) {
			$(this).addClass('is-active');
		} else {
			$(this).removeClass('is-active');
		}
	});
	// collapse 작동
	$(document).on('click', '[data-action="collapse"]', function (event) {
		event = event || window.event;
		event.preventDefault ? event.preventDefault() : (event.returnValue = false);
		var $target = $($(this).attr('href'));
		// 아코디언 형태인 경우
		if ($target.data('parent') !== undefined && $target.data('parent') != '') {
			var accordionParent = $target.data('parent');

			if ($(this).hasClass('is-active')) {
				$target
					.closest('#' + accordionParent)
					.find('.is-collapsible')
					.each(function () {
						if ($(this).hasClass('is-active')) {
							$(this).attr({ 'aria-expanded': 'false' }).removeClass('is-active').slideUp('fast');
						}
					});
				$(this).removeClass('is-active');
				$target.attr({ 'aria-expanded': 'false' }).removeClass('is-active').slideUp('fast');
			} else {
				$target
					.closest('#' + accordionParent)
					.find('[data-action="collapse"]')
					.removeClass('is-active');
				$target
					.closest('#' + accordionParent)
					.find('.is-collapsible')
					.removeClass('is-active')
					.delay(10)
					.slideUp('fast');
				$(this).addClass('is-active');
				$target.attr({ 'aria-expanded': 'true' }).addClass('is-active').stop().slideDown('fast');
			}
		}
		// 일반 collapse 인 경우
		else {
			if ($(this).hasClass('is-active')) {
				$(this).removeClass('is-active');
				$target.attr({ 'aria-expanded': 'false' }).removeClass('is-active').slideUp('fast');
			} else {
				$(this).addClass('is-active');
				$target.attr({ 'aria-expanded': 'true' }).addClass('is-active').stop().slideDown('fast');
			}
		}
	});
});

// 장바구니 수량 증감
$(document)
	.on('click', '.spinner-box:not(.spinner-box__not) .spinner-box__plus', function (e) {
		var countVal = parseInt($(this).closest('.spinner-box').find('input[type=number]').val());
		var toCountVal = countVal + 1;
		if (toCountVal > 99) {
			alert('구매수량은 100개 이상이 될 수 없습니다.');
		} else if (toCountVal <= 0) {
			alert('구매수량은 1개 이상이어야 합니다.');
		} else {
			$(this).closest('.spinner-box').find('input[type=number]').val(toCountVal);
		}
	})
	.on('click', '.spinner-box:not(.spinner-box__not) .spinner-box__minus', function (e) {
		var countVal = parseInt($(this).closest('.spinner-box').find('input[type=number]').val());
		var toCountVal = countVal - 1;
		if (toCountVal > 99) {
			alert('구매수량은 100개 이상이 될 수 없습니다.');
		} else if (toCountVal <= 0) {
			alert('구매수량은 1개 이상이어야 합니다.');
		} else {
			$(this).closest('.spinner-box').find('input[type=number]').val(toCountVal);
		}
	});

function tabActivate(thisEl) {
	thisEl.closest('[role="tablist"]').children().children('[aria-selected="true"]').attr({
		tabindex: '-1',
		'aria-selected': 'false',
	});
	thisEl.parent().addClass('is-active').siblings().removeClass('is-active');
	thisEl.attr({
		tabindex: '0',
		'aria-selected': 'true',
	});
	// 연관된 탭 패널 활성화
	$('#' + thisEl.attr('aria-controls'))
		.removeAttr('hidden')
		.attr('tabindex', '0')
		.addClass('is-active')
		// 기존 탭 패널 비활성화
		.siblings('.tabpanel')
		.attr({ tabindex: '-1', hidden: '' })
		.removeClass('is-active');
}
// E : Dynamic Tab + Content -- jQuery

// S : fixed 스크롤 체크 - 20200601
var isIE = /*@cc_on!@*/ false || !!document.documentMode;
var position;
// IE11 을 제외하고 resize (left-menu 같은 높이값이 높은 요소에만 적용)
if (!isIE && $('.left-menu').length > 0 && $('#container').length > 0 && $('#footer').length > 0 && $('.location-header').length > 0) {
	if (!(typeof ResizeObserver === 'undefined')) {
		let resizeObserver = new ResizeObserver(function () {
			var footer_top = $('#footer').offset().top;
			var scroll_top = $(window).scrollTop();
			var container_height = $('#container').innerHeight() - $('.location-header').height() - 50;
			check_scroll_fixed('', '.section-contents-wrap', '.left-menu');
			if (container_height < $('.left-menu').outerHeight()) {
				$('.left-menu').css({ position: 'relative', top: 'unset' });
			}
		});
		const elem = document.querySelector('.left-menu');
		resizeObserver.observe(elem);
	}
}
// scroll Up and Down Check
function check_scroll_updown(scr, ths) {
	if (scr > position) {
		// scrollDown Check
		ths.css({ position: 'fixed', top: $(window).innerHeight() - ths.outerHeight() + 'px' });
	} else {
		// scrollUp	Check
		ths.css({ position: 'fixed', top: '0px' });
	}
	position = scr;
}
function check_scroll_fixed(_mode, _wrapper, _this) {
	// fixed 일 경우 true 반환
	// _mode 가 simple 일 경우는 left-menu 처럼 높이 값을 체크해서 position 변화할 필요가 없는 단순 고정
	var wrapper_top = $(_wrapper).offset().top;
	var scroll_top = $(window).scrollTop();
	var scroll_btm = scroll_top + $(window).innerHeight();
	if ($('#container').length > 0 && $('#footer').length > 0 && $('.location-header').length > 0) {
		var footer_top = $('#footer').offset().top;
		var container_height = $('#container').innerHeight() - $('.location-header').height() - 50;
		if (scroll_top > wrapper_top) {
			$(_wrapper).addClass('is-fixed');
			if (_mode.indexOf('simple') !== -1) {
				$(_this).css({ position: 'fixed', top: '0' });
				// 기획전, 이벤트 등 4뎁스 탭 padding-top 속성 수정 : 20200622
				if (_mode == 'simple-padding') {
					$(_wrapper).css({ 'padding-top': $(_this).outerHeight() + 'px' });
				}
			} else {
				// this-height 이 윈도 height 보다 작은 경우 스크롤 다운/업 이벤트 작동 중지
				if (container_height > $(_this).outerHeight() + 40) {
					if ($(_this).outerHeight() > footer_top - scroll_top) {
						check_scroll_updown(scroll_top, $(_this));
						if (scroll_btm > footer_top) {
							$(_wrapper).removeClass('is-fixed');
							$(_this).css({ top: -scroll_btm + footer_top - $(_this).outerHeight() + $(window).innerHeight() + 'px' });
						}
					} else {
						if ($(_this).outerHeight() > $(window).innerHeight()) {
							check_scroll_updown(scroll_top, $(_this));
						} else {
							$(_this).css({ position: 'fixed', top: '0' });
						}
					}
				}
			}
			if (_mode.indexOf('simple') !== -1 && scroll_top > footer_top - $(_this).outerHeight()) {
				$(_this).css({ position: 'relative', top: 'initial' });
				$(_wrapper).removeClass('is-fixed');
				// 기획전, 이벤트 등 4뎁스 탭 padding-top 속성 수정 : 20200622
				if (_mode == 'simple-padding') {
					$(_wrapper).css({ 'padding-top': '' });
				}
			}
		} else {
			$(_this).css({ position: 'relative', top: 'initial' });
			$(_wrapper).removeClass('is-fixed');
			// 4뎁스 탭 padding-top 속성 추가 : 20200616
			$(_wrapper).css({ 'padding-top': '' });
		}
	}
}
// IE11 의 경우 fixed 후 top 값을 변화시키는 스크롤 동작시 끊김현상이 심해 스크롤하지 않고 고정.
if ($('.left-menu').length > 0 && !isIE) {
	check_scroll_fixed('', '.section-contents-wrap', '.left-menu');

	$(window)
		.on('scroll', function () {
			check_scroll_fixed('', '.section-contents-wrap', '.left-menu');
		})
		.on('resize', function () {
			check_scroll_fixed('', '.section-contents-wrap', '.left-menu');
		});
}
if ($('.section-category-item__relation').length > 0) {
	check_scroll_fixed('simple', '.section-category-item-search', '.section-category-item__relation');

	$(window)
		.on('scroll', function () {
			check_scroll_fixed('simple', '.section-category-item-search', '.section-category-item__relation');
		})
		.on('resize', function () {
			check_scroll_fixed('simple', '.section-category-item-search', '.section-category-item__relation');
		});
}
// E : fixed 스크롤 체크

// S : a 태그의 href 를 이용해 현재 페이지 스크롤 처리 : 20200615
$(document).on('click', '.tab-block-item > a[href^="#"]', function (e) {
	// target element id
	var id = $(this).attr('href');

	// target element
	var $id = $(id);
	if ($id.length === 0) {
		return;
	}

	// prevent standard hash navigation (avoid blinking in IE)
	e.preventDefault();

	// top position relative to the document
	var pos = $id.offset().top;

	// animated top scrolling
	$('body, html').animate({ scrollTop: pos });
});
// E : a 태그의 href 를 이용해 현재 페이지 스크롤 처리

// S : Dropdowns
// Functions

function getAll(selector) {
	return Array.prototype.slice.call(document.querySelectorAll(selector), 0);
}

var $dropdowns = getAll('.dropdown:not(.is-hoverable)');
$(document)
	.on('click', function (e) {
		if (!$(e.target).closest('.dropdown').length > 0) {
			$('.dropdown.is-active').removeClass('is-active');
		}
	})
	.on('mouseenter', '.dropdown-trigger .button', function (e) {
		$(this).closest('.dropdown').addClass('is-hover');
	})
	.on('mouseleave', '.dropdown-trigger .button', function (e) {
		$(this).closest('.dropdown').removeClass('is-hover');
	})
	.on('click', '.dropdown-trigger .button', function (e) {
		$('.dropdown:not(.is-hover)').removeClass('is-active');
		e.preventDefault();
		if ($(this).closest('.dropdown').hasClass('is-active')) {
			$(this).closest('.dropdown').removeClass('is-active');
		} else {
			$(this).closest('.dropdown').addClass('is-active');
		}
	})
	.on('click', '.dropdown-item', function (e) {
		// 드롭다운 관련 추가 : 드롭다운 dropdown 아이템 이벤트 처리
		if ($(this).hasClass('is-icon')) {
			$(this).addClass('is-active').siblings().removeClass('is-active');
			$(this).closest('.dropdown').find('[aria-controls="dropdown-menu"]').children('span').eq(0).empty().append($(this).children().clone());
			$(this).closest('.dropdown').find('[aria-controls="dropdown-menu"]').children('span').eq(0).append($(this).text());
		} else {
			$(this).addClass('is-active').siblings().removeClass('is-active');
			$(this).closest('.dropdown').find('[aria-controls="dropdown-menu"]').children('span').eq(0).text($(this).text());
		}

		var dropdownWidth = $(this).outerWidth();
		if ($('.header-search__dropdown').length > 0) {
			$(this).closest('.header-search__dropdown').css('min-width', dropdownWidth).find('.dropdown').css('width', 'auto');
		}
		$('.dropdown.is-active').removeClass('is-active');
	});

function closeDropdowns() {
	$dropdowns.forEach(function ($el) {
		$el.classList.remove('is-active');
	});
}
// E : Dropdowns
// Modals : 20200602
// Modal Open
$(document).on('click', '.modal-button', function (e) {
	e.preventDefault();
	var target = '#' + $(this).data('target');
	// $(this).addClass('is-clipped');
	$('html').addClass('is-clipped');
	$(target).addClass('is-active');
});
// Modal Close
$(document).on('click', '.modal-background, .modal-close, .modal-card-head .delete, .modal .button-modal-close', function (e) {
	closeModals();
});
function closeModals() {
	$('html').removeClass('is-clipped');
	$(document).find('.modal').removeClass('is-active');
}
$(document).on('keydown', function (event) {
	var e = event || window.event;
	if (e.keyCode === 27) {
		closeModals();
		closeDropdowns();
	}
});
// document.addEventListener('keydown', function (event) {
// 	var e = event || window.event;
// 	if (e.keyCode === 27) {
// 		closeModals();
// 		closeDropdowns();
// 	}
// });

// S : 상단 헤더 통합검색 바
// sample

$(document)
	.on('click', function (e) {
		if (!$(e.target).closest('.header-search-wrap').length > 0) {
			$('.header-search-content').hide();
		}
		if ($(e.target).closest('.dropdown-wrap').length > 0) {
			$('.header-search-content').hide();
		}
	})
	.on('click', '.header-search__input', function (e) {
		e.preventDefault();
		$('#headerSearchContent').show();
	})
	.on('click', '.button-search-content__del', function (e) {
		e.stopPropagation();
		$(this).closest('li').remove();
	})
	.on('click', '.button-search-all__del', function (e) {
		e.preventDefault();
		$(this).closest('.tabpanel').find('.header-search-content__body li').remove();
	})
	.on('click', '.button-search-content__close', function (e) {
		e.preventDefault();
		$(this).closest('.header-search-content').hide();
	})
	.on('keyup', '.header-search__input', function (e) {
		if ($(this).val() != '') {
			$('#headerSearchContent').hide();
			$('#headerSearchKeyword').show();
		} else {
			$('#headerSearchContent').show();
			$('#headerSearchKeyword').hide();
		}
	});
// E : 상단 헤더 통합검색 바

document.addEventListener('DOMContentLoaded', function () {
	// Cookies

	var cookieBookModalName = 'bulma_closed_book_modal';
	var cookieBookModal = Cookies.getJSON(cookieBookModalName) || false;

	// Sidebar links

	var $categories = getAll('#categories .bd-category');

	if ($categories.length > 0) {
		$categories.forEach(function (el) {
			var toggle_el = el.querySelector('.bd-category-toggle');

			toggle_el.addEventListener('click', function (event) {
				// closeCategories(el);
				el.classList.toggle('is-active');
			});
		});
	}

	function closeCategories(current_el) {
		$categories.forEach(function (el) {
			if (current_el == el) {
				return;
			}
			el.classList.remove('is-active');
		});
	}

	var anchors_ref_el = document.getElementById('anchorsReference');
	var anchors_el = document.getElementById('anchors');
	var anchor_links_el = getAll('.bd-anchor-link');

	var anchors_by_id = {};
	var anchors_order = [];
	var anchor_nav_els = [];

	if (anchors_el && anchor_links_el.length > 0) {
		anchors_el.classList.add('is-active');
		var anchors_el_list = anchors_el.querySelector('.bd-anchors-list');

		anchor_links_el.forEach(function (el, index) {
			var link_target = el.getAttribute('href');
			var link_text = el.previousElementSibling.innerText;

			if (link_text != '') {
				var item_el = createAnchorLink(link_text, link_target);
				anchors_el_list.appendChild(item_el);

				var anchor_key = link_target.substring(1); // #target -> target
				anchors_by_id[anchor_key] = {
					id: anchor_key,
					index: index,
					target: link_target,
					text: link_text,
					nav_el: item_el,
				};
				anchors_order.push(anchor_key);
				anchor_nav_els.push(item_el);
			}
		});

		var back_to_top_el = createAnchorLink('Back to top', '');
		back_to_top_el.onclick = scrollToTop;
		anchors_el_list.appendChild(back_to_top_el);
	}

	function scrollToTop() {
		window.scrollTo(0, 0);
	}

	function createAnchorLink(text, target) {
		var item_el = document.createElement('li');
		var link_el = document.createElement('a');
		var text_node = document.createTextNode(text);

		if (target) {
			link_el.setAttribute('href', target);
		}

		link_el.appendChild(text_node);
		item_el.appendChild(link_el);

		return item_el;
	}

	function closeCategories(current_el) {
		$categories.forEach(function (el) {
			if (current_el == el) {
				return;
			}
			el.classList.remove('is-active');
		});
	}

	// Toggles
	var $burgers = getAll('.burger');

	if ($burgers.length > 0) {
		$burgers.forEach(function ($el) {
			$el.addEventListener('click', function () {
				var target = $el.dataset.target;
				var $target = document.getElementById(target);
				$el.classList.toggle('is-active');
				$target.classList.toggle('is-active');
			});
		});
	}

	// Clipboard
	var $highlights = getAll('.highlight');
	var itemsProcessed = 0;

	if ($highlights.length > 0) {
		$highlights.forEach(function ($el) {
			var copyEl = '<button class="button is-small bd-copy">Copy</button>';
			var expandEl = '<button class="button is-small bd-expand">Expand</button>';
			$el.insertAdjacentHTML('beforeend', copyEl);

			var $parent = $el.parentNode;
			if ($parent && $parent.classList.contains('bd-is-more')) {
				var showEl = '<button class="bd-show"><div><span class="icon"><i class="fas fa-code"></i></span> <strong>Show code</strong></div></button>';
				$el.insertAdjacentHTML('beforeend', showEl);
			} else if ($el.firstElementChild.scrollHeight > 480 && $el.firstElementChild.clientHeight <= 480) {
				$el.insertAdjacentHTML('beforeend', expandEl);
			}

			itemsProcessed++;
			if (itemsProcessed === $highlights.length) {
				addHighlightControls();
			}
		});
	}

	function addHighlightControls() {
		var $highlightButtons = getAll('.highlight .bd-copy, .highlight .bd-expand');

		$highlightButtons.forEach(function ($el) {
			$el.addEventListener('mouseenter', function () {
				$el.parentNode.classList.add('bd-is-hovering');
			});

			$el.addEventListener('mouseleave', function () {
				$el.parentNode.classList.remove('bd-is-hovering');
			});
		});

		var $highlightExpands = getAll('.highlight .bd-expand');

		$highlightExpands.forEach(function ($el) {
			$el.addEventListener('click', function () {
				$el.parentNode.firstElementChild.style.maxHeight = 'none';
			});
		});

		var $highlightShows = getAll('.highlight .bd-show');

		$highlightShows.forEach(function ($el) {
			$el.addEventListener('click', function () {
				$el.parentNode.parentNode.classList.remove('bd-is-more-clipped');
			});
		});
	}

	// Scrolling

	var html_el = document.documentElement;
	var navbarEl = document.getElementById('navbar');
	var navbarBurger = document.getElementById('navbarBurger');
	var specialShadow = document.getElementById('specialShadow');
	var NAVBAR_HEIGHT = 52;
	var THRESHOLD = 160;
	var horizon = NAVBAR_HEIGHT;
	var whereYouStoppedScrolling = 0;
	var scrollFactor = 0;
	var currentTranslate = 0;

	// Anchors highlight

	var past_anchors = [];
	anchor_links_el.reverse();
	var trigger_offset = 24; // In pixels
	var typo_el = document.getElementById('typo');

	function whenScrolling() {
		if (anchors_ref_el) {
			var bounds = anchors_ref_el.getBoundingClientRect();
			var anchors_height = anchors_el.clientHeight;
			var typo_bounds = typo_el.getBoundingClientRect();
			var typo_height = typo_el.clientHeight;

			if (bounds.top < 1 && typo_bounds.top - anchors_height + typo_height > 0) {
				anchors_el.classList.add('is-pinned');
			} else {
				anchors_el.classList.remove('is-pinned');
			}

			anchor_links_el.some(function (el) {
				var bounds = el.getBoundingClientRect();
				var href = el.getAttribute('href');
				var key = href.substring(1); // #target -> target

				if (bounds.top < 1 + trigger_offset && past_anchors.indexOf(key) == -1) {
					past_anchors.push(key);
					highlightAnchor();
					return;
				} else if (bounds.top > 0 + trigger_offset && past_anchors.indexOf(key) != -1) {
					removeFromArray(past_anchors, key);
					highlightAnchor();
					return;
				}
			});
		}
	}

	function highlightAnchor() {
		var future_anchors = anchors_order.diff(past_anchors);
		var highest_index = -1;
		var highest_anchor_key = '';

		if (past_anchors.length > 0) {
			past_anchors.forEach(function (key, index) {
				var anchor = anchors_by_id[key];
				anchor.nav_el.className = 'is-past';

				// Keep track of the bottom most item
				if (anchor.index > highest_index) {
					highest_index = anchor.index;
					highest_anchor_key = key;
				}
			});

			if (highest_anchor_key in anchors_by_id) {
				anchors_by_id[highest_anchor_key].nav_el.className = 'is-current';
			}
		}

		if (future_anchors.length > 0) {
			future_anchors.forEach(function (key, index) {
				var anchor = anchors_by_id[key];
				anchor.nav_el.className = '';
			});
		}
	}

	// Scroll

	function upOrDown(lastY, currentY) {
		if (currentY >= lastY) {
			return goingDown(currentY);
		}
		return goingUp(currentY);
	}

	function goingDown(currentY) {
		var trigger = NAVBAR_HEIGHT;
		whereYouStoppedScrolling = currentY;

		if (currentY > horizon) {
			horizon = currentY;
		}

		translateHeader(currentY, false);
	}

	function goingUp(currentY) {
		var trigger = 0;

		if (currentY < whereYouStoppedScrolling - NAVBAR_HEIGHT) {
			horizon = currentY + NAVBAR_HEIGHT;
		}

		translateHeader(currentY, true);
	}

	function constrainDelta(delta) {
		return Math.max(0, Math.min(delta, NAVBAR_HEIGHT));
	}

	function translateHeader(currentY, upwards) {
		// let topTranslateValue;
		var translateValue = void 0;

		if (upwards && currentTranslate == 0) {
			translateValue = 0;
		} else if (currentY <= NAVBAR_HEIGHT) {
			translateValue = currentY * -1;
		} else {
			var delta = constrainDelta(Math.abs(currentY - horizon));
			translateValue = delta - NAVBAR_HEIGHT;
		}

		if (translateValue != currentTranslate) {
			var navbarStyle = '\n        transform: translateY(' + translateValue + 'px);\n      ';
			currentTranslate = translateValue;
			navbarEl.setAttribute('style', navbarStyle);
		}

		if (currentY > THRESHOLD * 2) {
			scrollFactor = 1;
		} else if (currentY > THRESHOLD) {
			scrollFactor = (currentY - THRESHOLD) / THRESHOLD;
		} else {
			scrollFactor = 0;
		}

		var translateFactor = 1 + translateValue / NAVBAR_HEIGHT;

		if (specialShadow) {
			specialShadow.style.opacity = scrollFactor;
			specialShadow.style.transform = 'scaleY(' + translateFactor + ')';
		}
	}

	var ticking = false;
	var lastY = 0;

	window.addEventListener('scroll', function () {
		var currentY = window.scrollY;

		if (!ticking) {
			window.requestAnimationFrame(function () {
				// upOrDown(lastY, currentY);
				whenScrolling();
				ticking = false;
				lastY = currentY;
			});
		}

		ticking = true;
	});

	// Utils

	function removeFromArray(array, value) {
		if (array.includes(value)) {
			var value_index = array.indexOf(value);
			array.splice(value_index, 1);
		}

		return array;
	}

	Array.prototype.diff = function (a) {
		return this.filter(function (i) {
			return a.indexOf(i) < 0;
		});
	};
});
