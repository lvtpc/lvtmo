// S : 메인타임세일MO1-1단
function showSwiperDateFormat(dt) {
	var dformat = dt.split('-');
	var days = Number(dformat[0]);
	var hours = Number(dformat[1]);
	days = days < 10 ? '0' + days : days;
	hours = hours < 10 ? '0' + hours : hours;

	if (days < 1) {
		if (hours < 1) {
			innerHTML =
				'<p><strong class="timesale-header-timer__num">마감임박</strong></p>';
		} else {
			innerHTML =
				'<p><strong class="timesale-header-timer__num">' +
				hours +
				'</strong><span class="timesale-header-timer__unit">h</span></p>';
		}
	} else {
		innerHTML = '<p class="timesale-header-timer__unit">DAY</p>';
		innerHTML += '<p class="timesale-header-timer__num">' + days + '</p>';
		innerHTML += '<p class="space">/</p>';
		innerHTML +=
			'<p><strong class="timesale-header-timer__num">' +
			hours +
			'</strong> <span class="timesale-header-timer__unit"> h</span></p>';
	}
	$('.timesale-header-timer').html(innerHTML);
}
function loadSwiperTimesale01(el) {
	var swiperList = $(el + ' .swiper-slide');
	if (swiperList.length > 1) {
		var swiperObj = new Swiper(el + ' .swiper-container', {
			// loop: true,
			// simulateTouch: false,
			autoplay: {
				delay: 5000,
			},
			pagination: {
				el: el + ' .swiper-pagination',
				type: 'progressbar',
			},
			navigation: {
				nextEl: el + ' .swiper-button-next',
				prevEl: el + ' .swiper-button-prev',
			},
			paginationClickable: true,
			centeredSlides: true,
			slidesPerView: 'auto',
			spaceBetween: 0,
			on: {
				init: function () {
					var swiper = this;
					var currentNum;
					var $this = $(el + ' .swiper-container');
					var duplicateNum = Number(
						$this.find('.swiper-slide-duplicate').length
					);
					var totalNum = swiper.slides.length - duplicateNum;
					// swiper loop 상태에 따라 current index : 200818
					if (swiper.params.loop) {
						currentNum =
							$this
								.find('.swiper-slide-active')
								.data('swiper-slide-index') + 1; // loop : true 일 경우
					} else {
						currentNum = this.activeIndex + 1; // loop : false 일 경우
					}
					totalNum = '0' + totalNum;
					currentNum = '0' + currentNum;
					$this
						.find('.swiper-pagination-total')
						.text(totalNum.slice(-2));
					$this
						.find('.swiper-pagination-current')
						.text(currentNum.slice(-2));
					// 반짝특가용 카운트다운 타이머
					var dateFormat = $this
						.find('.swiper-slide-active')
						.children('.product-item')
						.data('format');
					showSwiperDateFormat(dateFormat);
				},
				slideChangeTransitionStart: function () {
					var swiper = this;
					var currentNum;
					var $this = $(el + ' .swiper-container');
					var duplicateNum = Number(
						$this.find('.swiper-slide-duplicate').length
					);
					var totalNum = swiper.slides.length - duplicateNum;
					// swiper loop 상태에 따라 current index : 200818
					if (swiper.params.loop) {
						currentNum =
							$this
								.find('.swiper-slide-active')
								.data('swiper-slide-index') + 1; // loop : true 일 경우
					} else {
						currentNum = this.activeIndex + 1; // loop : false 일 경우
					}
					totalNum = '0' + totalNum;
					currentNum = '0' + currentNum;
					$this
						.find('.swiper-pagination-total')
						.text(totalNum.slice(-2));
					$this
						.find('.swiper-pagination-current')
						.text(currentNum.slice(-2));
					// 반짝특가용 카운트다운 타이머
					var dateFormat = $this
						.find('.swiper-slide-active')
						.children('.product-item')
						.data('format');
					showSwiperDateFormat(dateFormat);
				},
				slideChangeTransitionEnd: function () {
					var swiper = this;
					var $this = $(el + ' .swiper-container');
					var sButton = $this.find('.swiper-button-pause');
					if (sButton.length) {
						if (sButton.is('.__state-swiper-pause')) {
							swiper.autoplay.start();
						}
					}
				},
			},
		});

		$(document)
			.on('click', el + ' .swiper-button-pause', function () {
				if ($(this).is('.__state-swiper-play')) {
					$(this)
						.removeClass('__state-swiper-play')
						.addClass('__state-swiper-pause')
						.find('i')
						.removeClass('ico-control-play')
						.addClass('ico-control-pause');
					swiperObj.autoplay.start();
				} else {
					$(this)
						.removeClass('__state-swiper-pause')
						.addClass('__state-swiper-play')
						.find('i')
						.removeClass('ico-control-pause')
						.addClass('ico-control-play');
					swiperObj.autoplay.stop();
				}
			})
			.on(
				'click',
				el + ' .swiper-button-prev,' + el + ' .swiper-button-next',
				function () {
					var isPlayControl = $(this)
						.closest('.section-category-control-wrap')
						.find('.swiper-button-pause');
					if (isPlayControl.is('.__state-swiper-play')) {
						isPlayControl
							.removeClass('__state-swiper-play')
							.addClass('__state-swiper-pause')
							.find('i')
							.removeClass('ico-control-play')
							.addClass('ico-control-pause');
					}
					swiperObj.autoplay.start();
				}
			);
	} else {
		swiperList.show();
	}
}
// E : 메인타임세일MO1-1단

// S : 메인타임세일MO2-1단
// var timer;
var timerIdNum;
var timerId = [];
function CountDownTimer(mode, dtimer, el, tid) {
	var end = new Date(dtimer);
	var _second = 1000;
	var _minute = _second * 60;
	var _hour = _minute * 60;
	var _day = _hour * 24;
	var innerHTML;

	// 타이머 형식 수정 : 200720
	// 1시간 이하로 남을 경우 00분 00초 남음 으로 수정
	function showRemaining() {
		var now = new Date();
		var distance = end - now;
		if (distance < 0) {
			clearInterval(timerId[tid]);
			el.find('.countdown-timer1').html('');
			el.find('.countdown-timer').html('');
			return;
		}
		var days = Math.floor(distance / _day);
		var hours = Math.floor((distance % _day) / _hour);
		var minutes = '0' + Math.floor((distance % _hour) / _minute);
		var seconds = '0' + Math.floor((distance % _minute) / _second);

		if (mode == 'open') {
			// 오픈 이후 : countdown-timer1
			if (days > 0) {
				days = days < 10 ? '0' + days : days;
				hours = hours < 10 ? '0' + hours : hours;
				innerHTML = '<i class="icon2-timer"></i> ';
				innerHTML +=
					'<strong class="groupbuy-item-goal__day">' +
					days +
					'</strong>일 ';
				innerHTML +=
					'<span class="groupbuy-item-goal__num"><strong>' +
					hours +
					'</strong>시간 <strong>남음</strong></span>';
				// innerHTML += '<span class="groupbuy-item-goal__num">' + hours.slice(-2) + ':' + minutes.slice(-2) + ':' + seconds.slice(-2) + ' 남음</span>';
			} else if (hours < 1) {
				minutes = minutes < 10 ? '0' + minutes : minutes;
				seconds = seconds < 10 ? '0' + seconds : seconds;
				innerHTML =
					'<span class="groupbuy-item-goal__num">' +
					minutes +
					'분 ' +
					seconds +
					'초 남음</span>';
			} else {
				hours = hours < 10 ? '0' + hours : hours;
				innerHTML +=
					'<span class="groupbuy-item-goal__num"><strong>' +
					hours +
					'</strong>시간 <strong>남음</strong></span>';
				// innerHTML = '<span class="timer">' + hours.slice(-2) + ':' + minutes.slice(-2) + ':' + seconds.slice(-2) + ' 남음</span>';
			}
			el.find('.countdown-timer1').html(innerHTML);
		} else {
			// 오픈 전 : countdown-timer
			if (days > 0) {
				days = days < 10 ? '0' + days : days;
				hours = hours < 10 ? '0' + hours : hours;
				innerHTML =
					'<strong class="groupbuy-item-goal__day">' +
					days +
					'</strong>일 ';
				innerHTML +=
					'<span class="groupbuy-item-goal__num"><strong>' +
					hours +
					'</strong>시간 <strong>남음</strong></span>';
			} else if (hours < 1) {
				minutes = minutes < 10 ? '0' + minutes : minutes;
				seconds = seconds < 10 ? '0' + seconds : seconds;
				innerHTML =
					'<span class="groupbuy-item-goal__num">' +
					minutes +
					'분 ' +
					seconds +
					'초 <strong>남음</strong></span>';
			} else {
				hours = hours < 10 ? '0' + hours : hours;
				innerHTML +=
					'<span class="groupbuy-item-goal__num"><strong>' +
					hours +
					'</strong>시간 <strong>남음</strong></span>';
			}
			el.find('.countdown-timer').html(innerHTML);
		}
	}
	clearInterval(timerId[tid]);
	showRemaining();
	// 1분에 1회 트리거
	timerId[tid] = setInterval(showRemaining, 60 * 1000);
}
timerIdNum = 0;
$(document).ready(function () {
	if ($('[data-timer]').length > 0) {
		$('[data-timer]').each(function (e) {
			// console.log('');
			var thisTimer = $(this).data('timer');
			var thisOpen = $(this).data('open');
			CountDownTimer(thisOpen, thisTimer, $(this), timerIdNum);
			timerIdNum++;
		});
	}
});
// 반짝 특가 버튼 event
$(document).on(
	'click',
	'.product-item-button__notification .button',
	function () {
		$(this).toggleClass('is-active');
		if ($(this).hasClass('is-active')) {
			$(this).children('i').attr('class', 'icon2-alarm-on');
		} else {
			$(this).children('i').attr('class', 'icon2-alarm-off');
		}
	}
);
// E : 메인타임세일MO2-1단

// 메인키비쥬얼 01
function loadSwiperKeyvisual01(el, initialNum) {
	// 배너 순서 지정
	if (initialNum == undefined) {
		initialNum = 0;
	}
	var swiperKeyVisual = new Swiper(el + ' .swiper-container', {
		loop: true,
		autoplay: {
			delay: 5000,
		},
		speed: 1000,
		// speed: 600,
		parallax: true,
		// grabCursor: true,
		watchSlidesProgress: true,
		mousewheelControl: false,
		keyboardControl: true,

		pagination: {
			el: el + ' .swiper-pagination',
			type: 'fraction',
		},
		navigation: {
			nextEl: el + ' .swiper-button-next',
			prevEl: el + ' .swiper-button-prev',
		},
		on: {
			slideChangeTransitionStart: function () {
				var swiper = this;
				var $this = $(el + ' .swiper-container');
				var duplicateNum = $this.find('.swiper-slide-duplicate').length;
				var totalNum = swiper.slides.length - duplicateNum;
				var currentNum =
					$this
						.find('.swiper-slide-active')
						.data('swiper-slide-index') + 1;
				$this.find('.swiper-pagination-total').text(totalNum);
				$this.find('.swiper-pagination-current').text(currentNum);
			},
			slideChangeTransitionEnd: function () {
				var swiper = this;
				var $this = $(el + ' .swiper-container');
				var sButton = $this.find('.swiper-button-pause');
				if (sButton.length) {
					if (sButton.is('.__state-swiper-play')) {
						swiper.autoplay.stop();
					} else {
						swiper.autoplay.start();
					}
				}
			},
		},
		initialSlide: initialNum,
	});
	$(document).on('click', el + ' .swiper-button-pause', function () {
		if ($(this).is('.__state-swiper-play')) {
			$(this)
				.removeClass('__state-swiper-play')
				.addClass('__state-swiper-pause')
				.find('i')
				.removeClass('ico-control-play')
				.addClass('ico-control-pause');
			swiperKeyVisual.autoplay.start();
		} else {
			$(this)
				.removeClass('__state-swiper-pause')
				.addClass('__state-swiper-play')
				.find('i')
				.removeClass('ico-control-pause')
				.addClass('ico-control-play');
			swiperKeyVisual.autoplay.stop();
		}
	});
}

// 메인키비쥬얼MO2
function loadSwiperKeyvisual02(el) {
	var swiperList = $(el + ' .swiper-slide'),
		swiperControlWrap = $(el + ' .swiper-control-box-wrap');

	if (swiperList.length > 1) {
		var swiperObj = new Swiper(el + ' .swiper-container', {
			autoplay: {
				delay: 5000,
			},
			loop: true,
			navigation: {
				nextEl: el + ' .swiper-button-next',
				prevEl: el + ' .swiper-button-prev',
			},
			pagination: {
				el: el + ' .swiper-pagination',
				type: 'fraction',
			},
			on: {
				slideChangeTransitionEnd: function () {
					var swiper = this;
					var $this = $(el + ' .swiper-container');
					var sButton = $this.find('.swiper-button-pause');
					if (sButton.length) {
						if (sButton.is('.__state-swiper-play')) {
							swiper.autoplay.stop();
						} else {
							swiper.autoplay.start();
						}
					}
				},
			},
		});
		$(document).on('click', el + ' .swiper-button-pause', function () {
			if ($(this).is('.__state-swiper-play')) {
				$(this)
					.removeClass('__state-swiper-play')
					.addClass('__state-swiper-pause')
					.find('i')
					.removeClass('ico-control-play')
					.addClass('ico-control-pause');
				swiperObj.autoplay.start();
			} else {
				$(this)
					.removeClass('__state-swiper-pause')
					.addClass('__state-swiper-play')
					.find('i')
					.removeClass('ico-control-pause')
					.addClass('ico-control-play');
				swiperObj.autoplay.stop();
			}
		});
	} else {
		swiperList.show();
		swiperControlWrap.hide();
	}
}

// 메인띠배너MO-빅와이드 + bullet
function loadSwiperWideBanner01(el) {
	var swiperList = $(el + ' .swiper-slide'),
		swiperControlWrap = $(el + ' .section-control-wrap');

	if (swiperList.length > 1) {
		var swiperObj = new Swiper(el + ' .swiper-container', {
			simulateTouch: false,
			speed: 800,
			loop: true,
			navigation: {
				nextEl: el + ' .swiper-button-next',
				prevEl: el + ' .swiper-button-prev',
			},
			pagination: {
				el: el + ' .swiper-pagination',
				clickable: true,
			},
		});
	} else {
		swiperList.show();
		swiperControlWrap.hide();
	}
}

// 메인특화배너MO5-쇼룸안내
function loadSwiperShowroom(el) {
	var swiperList = $(el + ' .swiper-slide'),
		swiperControlWrap = $(el + ' .section-control-wrap');

	if (swiperList.length > 1) {
		var swiperObj = new Swiper(el + ' .swiper-container', {
			loop: true,
			simulateTouch: false,
			speed: 800,
			navigation: {
				nextEl: el + ' .swiper-button-next',
				prevEl: el + ' .swiper-button-prev',
			},
			pagination: {
				el: el + ' .swiper-pagination',
				clickable: true,
			},
			on: {
				slideChangeTransitionStart: function () {
					var swiper = this;
					var $this = $(el + ' .swiper-container');
					var $thumb = $(el + ' .box-block-item-lists');
					var duplicateNum = $this.find(
						'.swiper-slide-duplicate'
					).length;
					var totalNum = swiper.slides.length - duplicateNum;
					if (swiper.params.loop) {
						currentNum = $this
							.find('.swiper-slide-active')
							.data('swiper-slide-index'); // loop : true 일 경우
					} else {
						currentNum = this.activeIndex; // loop : false 일 경우
					}
					$thumb
						.children('li')
						.eq(currentNum)
						.addClass('is-active')
						.siblings('li')
						.removeClass('is-active');
				},
			},
		});
	} else {
		swiperList.show();
		swiperControlWrap.hide();
	}
	$(document).on('click', el + ' .box-block-item', function (e) {
		e.preventDefault();
		$(this).addClass('is-active').siblings('li').removeClass('is-active');
		swiperObj.slideTo($(this).index() + 1);
	});
}

// 메인키비쥬얼 06
function loadSwiperKeyvisual06(el, initialNum) {
	// 배너 순서 지정
	if (initialNum == undefined) {
		initialNum = 0;
	}
	var swiperKeyVisual = new Swiper(
		el + ' .tab-swiper-container .swiper-container',
		{
			loop: true,
			// autoplay: {
			// 	delay: 5000,
			// },
			// speed: 1000,
			// speed: 600,
			parallax: true,
			// grabCursor: true,
			slidesPerView: 5,
			spaceBetween: 10,
			watchSlidesProgress: true,
			mousewheelControl: false,
			keyboardControl: true,
			pagination: {
				el: el + ' .swiper-pagination',
				type: 'progressbar',
			},
			navigation: {
				nextEl: el + ' .swiper-button-next',
				prevEl: el + ' .swiper-button-prev',
			},
			on: {
				slideChangeTransitionStart: function () {
					var swiper = this;
					var $this = $(el + ' .swiper-container');
					var duplicateNum = $this.find(
						'.swiper-slide-duplicate'
					).length;
					var totalNum = swiper.slides.length - duplicateNum;
					var currentNum =
						$this
							.find('.swiper-slide-active')
							.data('swiper-slide-index') + 1;
					$this.find('.swiper-pagination-total').text(totalNum);
					$this.find('.swiper-pagination-current').text(currentNum);
				},
				slideChangeTransitionEnd: function () {
					var swiper = this;
					var $this = $(el + ' .swiper-container');
					var sButton = $this.find('.swiper-button-pause');
					if (sButton.length) {
						if (sButton.is('.__state-swiper-play')) {
							swiper.autoplay.stop();
						} else {
							swiper.autoplay.start();
						}
					}
				},
			},
			initialSlide: initialNum,
		}
	);
	$(document).on('click', el + ' .swiper-button-pause', function () {
		if ($(this).is('.__state-swiper-play')) {
			$(this)
				.removeClass('__state-swiper-play')
				.addClass('__state-swiper-pause')
				.find('i')
				.removeClass('ico-control-play')
				.addClass('ico-control-pause');
			swiperKeyVisual.autoplay.start();
		} else {
			$(this)
				.removeClass('__state-swiper-pause')
				.addClass('__state-swiper-play')
				.find('i')
				.removeClass('ico-control-pause')
				.addClass('ico-control-play');
			swiperKeyVisual.autoplay.stop();
		}
	});
}

// 중앙 배열 프로그레스 표시 기본 모바일 스와이퍼
// ex) <div class="swiper-container">
function loadSwiperProgress(el) {
	var swiperList = $(el + ' .swiper-slide');
	if (swiperList.length > 1) {
		var swiperObj = new Swiper(el + ' .swiper-container', {
			loop: true,
			// simulateTouch: false,
			pagination: {
				el: el + ' .swiper-pagination',
				type: 'progressbar',
			},
			navigation: {
				nextEl: el + ' .swiper-button-next',
				prevEl: el + ' .swiper-button-prev',
			},
			paginationClickable: true,
			centeredSlides: true,
			slidesPerView: 1.2,
			spaceBetween: 10,
			observer: true,
			observeParents: true,
			on: {
				slideChangeTransitionStart: function () {
					var swiper = this;
					var $this = $(el + ' .swiper-container');
					var duplicateNum = $this.find(
						'.swiper-slide-duplicate'
					).length;
					var totalNum = swiper.slides.length - duplicateNum;
					if (swiper.params.loop) {
						currentNum =
							$this
								.find('.swiper-slide-active')
								.data('swiper-slide-index') + 1; // loop : true 일 경우
					} else {
						currentNum = this.activeIndex + 1; // loop : false 일 경우
					}
					totalNum = '0' + totalNum;
					currentNum = '0' + currentNum;
					$this
						.find('.swiper-pagination-total')
						.text(totalNum.slice(-2));
					$this
						.find('.swiper-pagination-current')
						.text(currentNum.slice(-2));
				},
			},
		});
	} else {
		swiperList.show();
	}
}

// 좌측부터 배열 프로그레스 표시 모바일 스와이퍼
// ex) <div class="swiper-container section">
function loadSwiperProgressLeft(el) {
	var swiperList = $(el + ' .swiper-slide'),
		swiperControlWrap = $(el + ' .paging-box');
	if (swiperList.length > 1) {
		var swiperObj = new Swiper(el + ' .swiper-container', {
			loop: true,
			// simulateTouch: false,
			pagination: {
				el: el + ' .swiper-pagination',
				type: 'progressbar',
			},
			navigation: {
				nextEl: el + ' .swiper-button-next',
				prevEl: el + ' .swiper-button-prev',
			},
			paginationClickable: true,
			// centeredSlides: true,
			slidesPerView: 1.2,
			spaceBetween: 10,
			observer: true,
			observeParents: true,
			on: {
				slideChangeTransitionStart: function () {
					var swiper = this;
					var $this = $(el + ' .swiper-container');
					var duplicateNum = $this.find(
						'.swiper-slide-duplicate'
					).length;
					var totalNum = swiper.slides.length - duplicateNum;
					if (swiper.params.loop) {
						currentNum =
							$this
								.find('.swiper-slide-active')
								.data('swiper-slide-index') + 1; // loop : true 일 경우
					} else {
						currentNum = this.activeIndex + 1; // loop : false 일 경우
					}
					totalNum = '0' + totalNum;
					currentNum = '0' + currentNum;
					$this
						.find('.swiper-pagination-total')
						.text(totalNum.slice(-2));
					$this
						.find('.swiper-pagination-current')
						.text(currentNum.slice(-2));
				},
			},
		});
	} else {
		swiperList.show();
		swiperControlWrap.hide();
	}
}

// autoplay 가 아닌 고정 3단 슬라이드
function loadSwiperCol3(el) {
	var swiperList = $(el + ' .swiper-slide'),
		swiperControlWrap = $(el + ' .paging-box');

	if (swiperList.length > 3) {
		var swiperObj = new Swiper(el + ' .swiper-container', {
			loop: false,
			navigation: {
				nextEl: el + ' .swiper-button-next',
				prevEl: el + ' .swiper-button-prev',
			},
			pagination: {
				el: el + ' .swiper-pagination',
				type: 'progressbar',
			},
			paginationClickable: true,
			spaceBetween: 0,
			slidesPerView: 3,
			slidesPerGroup: 3,
		});
	} else {
		swiperList.show();
		swiperControlWrap.hide();
	}
}

// autoplay 가 아닌 기본 슬라이드
function loadSwiperDefault(el) {
	var swiperList = $(el + ' .swiper-slide'),
		swiperControlWrap = $(el + ' .swiper-main-control');

	if (swiperList.length > 1) {
		var swiperObj = new Swiper(el + ' .swiper-container', {
			loop: false,
			navigation: {
				nextEl: el + ' .swiper-button-next',
				prevEl: el + ' .swiper-button-prev',
			},
			paginationClickable: true,
			spaceBetween: 0,
			slidesPerView: 1,
		});
	} else {
		swiperList.show();
		swiperControlWrap.hide();
	}
}

// 2단 프로그레스 표시 모바일 스와이퍼
function loadSwiperProgress2Col(el) {
	var swiperList = $(el + ' .swiper-slide');
	if (swiperList.length > 2) {
		var swiperObj = new Swiper(el + ' .swiper-container', {
			loop: true,
			// simulateTouch: false,
			pagination: {
				el: el + ' .swiper-pagination',
				type: 'progressbar',
			},
			paginationClickable: true,
			// centeredSlides: true,
			slidesPerView: 2,
			spaceBetween: 10,
			observer: true,
			observeParents: true,
		});
	} else {
		swiperList.show();
	}
}

// 오픈 하우스 해시태그 is-active
$(document).on('click', '.hash-tag__name', function () {
	$(this).addClass('is-active').siblings('a').removeClass('is-active');
});

// 공간 솔루션 : 200914
$(document)
	.on('click', '.svg-link-item', function () {
		var $thisTip = $('.solution-tooltip');
		var thisSol = $(this).data('solution');
		$thisTip.text($(this).data('name'));
		$('.solution-img-bg').removeClass('is-active');
		$('.solution-img-bg__' + thisSol).addClass('is-active');
		$('.solution-option-item > div').removeClass('is-active');
		$('#solution-option-item__' + thisSol)
			.addClass('is-active')
			.find('select option:eq(0)')
			.prop('selected', true);
		$('.solution-option-brand > div').removeClass('is-active');
		$('#solution-option-brand__' + thisSol)
			.addClass('is-active')
			.find('select option:eq(0)')
			.prop('selected', true);
	})
	.on('change', '.solution-option-menu select', function () {
		var dataSub = $(this).find('option:selected').attr('data-sub');
		var dataSol = $(this).find('option:selected').attr('data-solution');
		if (dataSub != '' && dataSub != undefined) {
			$('.solution-option-brand > div').removeClass('is-active');
			$('#solution-option-brand__' + dataSub)
				.addClass('is-active')
				.find('select option:eq(0)')
				.prop('selected', true);
		}
		if (dataSol != '' && dataSol != undefined) {
			$('.solution-option-brand > div').removeClass('is-active');
			$('#solution-option-brand__' + dataSol)
				.addClass('is-active')
				.find('select option:eq(0)')
				.prop('selected', true);
		}
	});
