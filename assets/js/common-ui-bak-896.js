'use strict';

// S: Dynamic Tab + Content -- jQuery
$(function () {
	// show/hide 처리
	$(document).on('click', '[role="show"]', function (event) {
		event = event || window.event;
		event.preventDefault
			? event.preventDefault()
			: (event.returnValue = false);

		// 선택된 탭 활성화
		$('#' + $(this).attr('aria-controls'))
			.removeAttr('hidden')
			.attr({ tabindex: '0', 'aria-hidden': 'false' })
			.addClass('is-active')
			// 기존 탭 패널 비활성화
			.siblings('.showpanel')
			.attr({ tabindex: '-1', 'aria-hidden': 'true', hidden: '' })
			.removeClass('is-active');
	});

	// let tabFocus = 0;
	$(document).on('click', '[role="tab"]', function (event) {
		event = event || window.event;
		event.preventDefault
			? event.preventDefault()
			: (event.returnValue = false);

		// 선택된 탭 활성화
		tabActivate($(this));
		// 탭 하위에 탭이 존재하는 경우 초기화 처리
		var $thisChildren = $('#' + $(this).attr('aria-controls')).find(
			'[role="tablist"]'
		);
		if ($thisChildren.length) {
			tabActivate(
				$thisChildren.children().eq(0).children('[role="tab"]')
			);
			$('#' + $(this).attr('aria-controls')).removeAttr('tabindex');
		}
	});

	$(document).on('keydown', '[role="tab"]', function (event) {
		event = event || window.event;
		var keycode = event.keyCode || event.which;
		var tabsLength = $(this).children('li').length;
		if (keycode === 39 || keycode === 37) {
			var firstEl = $(this)
				.closest('[role="tablist"]')
				.children('li')
				.eq(0)
				.children('[role="tab"]');
			var lastEl = $(this)
				.closest('[role="tablist"]')
				.children('li')
				.eq(tabsLength - 1)
				.children('[role="tab"]');
			$(this).parent('li').addClass('');
			if (keycode === 39) {
				$(this).parent('li').next().children('[role="tab"]').focus();
				if (!$(this).parent('li').next().length) {
					firstEl.focus();
				}
			} else if (keycode === 37) {
				$(this).parent('li').prev().children('[role="tab"]').focus();
				if (!$(this).parent('li').prev().length) {
					lastEl.focus();
				}
			}
		}
	});

	// 상품유닛용 툴팁 선언을 위한 스크립트 : 200701
	if ($('[data-toggle="tooltip"]').length > 0) {
		$('[data-toggle="tooltip"]').tooltip();
	}

	// Collapse 처리
	// 초기화
	$('.is-collapsible').each(function () {
		if ($(this).hasClass('is-active')) {
			$(this).attr({ 'aria-expanded': 'true' });
		} else {
			$(this).attr({ 'aria-expanded': 'false' }).css({ display: 'none' });
		}
	});
	$('[data-action="collapse"]').each(function () {
		if ($($(this).attr('href')).hasClass('is-active')) {
			$(this).addClass('is-active');
		} else {
			$(this).removeClass('is-active');
		}
	});
	// collapse 작동
	$(document).on('click', '[data-action="collapse"]', function (event) {
		event = event || window.event;
		event.preventDefault
			? event.preventDefault()
			: (event.returnValue = false);
		// 스크립트 추가 : 200803
		if (
			$(this).hasClass('pitem-option__button') &&
			$(this).closest('.pitem-option').length
		) {
			if ($(this).closest('.pitem-option').hasClass('is-active')) {
				$(this).closest('.pitem-option').removeClass('is-active');
			} else {
				$(this).closest('.pitem-option').addClass('is-active');
			}
		}
		var $target = $($(this).attr('href'));
		// 아코디언 형태인 경우
		if (
			$target.data('parent') !== undefined &&
			$target.data('parent') != ''
		) {
			var accordionParent = $target.data('parent');

			if ($(this).hasClass('is-active')) {
				$target
					.closest('#' + accordionParent)
					.find('.is-collapsible')
					.each(function () {
						if ($(this).hasClass('is-active')) {
							$(this)
								.attr({ 'aria-expanded': 'false' })
								.removeClass('is-active')
								.slideUp('fast');
						}
					});
				$(this).removeClass('is-active');
				$target
					.attr({ 'aria-expanded': 'false' })
					.removeClass('is-active')
					.slideUp('fast');
			} else {
				$target
					.closest('#' + accordionParent)
					.find('[data-action="collapse"]')
					.removeClass('is-active');
				$target
					.closest('#' + accordionParent)
					.find('.is-collapsible')
					.removeClass('is-active')
					.delay(10)
					.slideUp('fast');
				$(this).addClass('is-active');
				$target
					.attr({ 'aria-expanded': 'true' })
					.addClass('is-active')
					.stop()
					.slideDown('fast');
			}
		}
		// 일반 collapse 인 경우
		else {
			if ($(this).hasClass('is-active')) {
				$(this).removeClass('is-active');
				$target
					.attr({ 'aria-expanded': 'false' })
					.removeClass('is-active')
					.slideUp('fast');
			} else {
				$(this).addClass('is-active');
				$target
					.attr({ 'aria-expanded': 'true' })
					.addClass('is-active')
					.stop()
					.slideDown('fast');
			}
		}
	});

	// 모바일 하단에 fixed 버튼이 들어가는 경우 #container 에 mb7 클래스 부여 : margin-bottom:70px
	if ($('.fixed-area-bottom').length) {
		$('#container').addClass('mb7');
	}
});

// 상품상세 구매플로팅 toggle
$(document).on('click', '.pitem-header-btns-wrap .btn-toggle', function () {
	$(this).toggleClass('on').next().slideToggle('fast');
	$(this).closest('.pitem-header-btns-inner').toggleClass('on');
});

// 상품상세 구매플로팅 입고알림 toggle
$(document).on(
	'click',
	'.pitem-header-btns-wrap .soldout .btns-soldout .alarm-check',
	function () {
		// $(this).removeClass('.is-danger').addClass('.is-dark');
		if ($(this).is('.is-danger')) {
			$(this).removeClass('is-danger');
			$(this).addClass('is-dark');
			$(this).text('입고알림해지');
		} else if ($(this).is('.is-dark')) {
			$(this).removeClass('is-dark');
			$(this).addClass('is-danger');
			$(this).text('입고알림');
		}
	}
);

// footer contents toggle
$(document).on('click', '.footer-bottom-content__info--title', function () {
	$(this).toggleClass('on').next().slideToggle('fast');
});

// 장바구니 수량 증감
$(document)
	.on(
		'click',
		'.spinner-box:not(.spinner-box__not) .spinner-box__plus',
		function (e) {
			var countVal = parseInt(
				$(this).closest('.spinner-box').find('input[type=number]').val()
			);
			var toCountVal = countVal + 1;
			if (toCountVal > 99) {
				// alert('구매수량은 100개 이상이 될 수 없습니다.');
				$(this)
					.closest('.spinner-box')
					.find('input[type=number]')
					.val(99);
			} else if (toCountVal <= 1) {
				// alert('구매수량은 1개 이상이어야 합니다.');
				$(this)
					.closest('.spinner-box')
					.find('input[type=number]')
					.val(1);
			} else {
				$(this)
					.closest('.spinner-box')
					.find('input[type=number]')
					.val(toCountVal);
			}
		}
	)
	.on(
		'click',
		'.spinner-box:not(.spinner-box__not) .spinner-box__minus',
		function (e) {
			var countVal = parseInt(
				$(this).closest('.spinner-box').find('input[type=number]').val()
			);
			var toCountVal = countVal - 1;
			if (toCountVal > 99) {
				// alert('구매수량은 100개 이상이 될 수 없습니다.');
				$(this)
					.closest('.spinner-box')
					.find('input[type=number]')
					.val(99);
			} else if (toCountVal <= 1) {
				// alert('구매수량은 1개 이상이어야 합니다.');
				$(this)
					.closest('.spinner-box')
					.find('input[type=number]')
					.val(1);
			} else {
				$(this)
					.closest('.spinner-box')
					.find('input[type=number]')
					.val(toCountVal);
			}
		}
	);

function tabActivate(thisEl) {
	thisEl
		.closest('[role="tablist"]')
		.children()
		.children('[aria-selected="true"]')
		.attr({
			tabindex: '-1',
			'aria-selected': 'false',
		});
	thisEl.parent().addClass('is-active').siblings().removeClass('is-active');
	thisEl.attr({
		tabindex: '0',
		'aria-selected': 'true',
	});
	// 연관된 탭 패널 활성화
	$('#' + thisEl.attr('aria-controls'))
		.removeAttr('hidden')
		.attr('tabindex', '0')
		.addClass('is-active')
		// 기존 탭 패널 비활성화
		.siblings('.tabpanel')
		.attr({ tabindex: '-1', hidden: '' })
		.removeClass('is-active');
}
// E : Dynamic Tab + Content -- jQuery

// S : fixed 스크롤 체크 - 20200601
// var isIE = /*@cc_on!@*/ false || !!document.documentMode;
// var position;
// if (!isIE && $('.left-menu').length > 0 && $('#container').length > 0 && $('#footer').length > 0 && $('.location-header').length > 0) {
// 	if (!(typeof ResizeObserver === 'undefined')) {
// 		let resizeObserver = new ResizeObserver(function () {
// 			var footer_top = $('#footer').offset().top;
// 			var scroll_top = $(window).scrollTop();
// 			var container_height = $('#container').innerHeight() - $('.location-header').height() - 50;
// 			check_scroll_fixed('', '.section-contents-wrap', '.left-menu');
// 			if (container_height < $('.left-menu').outerHeight()) {
// 				$('.left-menu').css({ position: 'relative', top: 'unset' });
// 			}
// 		});
// 		const elem = document.querySelector('.left-menu');
// 		resizeObserver.observe(elem);
// 	}
// }
// scroll Up and Down Check
function check_scroll_updown(scr, ths) {
	if (scr > position) {
		// scrollDown Check
		ths.css({
			position: 'fixed',
			top: $(window).innerHeight() - ths.outerHeight() + 'px',
		});
		// console.log('down');
	} else {
		// scrollUp	Check
		ths.css({ position: 'fixed', top: '0px' });
		// console.log('up');
	}
	position = scr;
}

// mobile header fixed (ios scroll bounce issue)
// mobile footer app bar
var lastScrollTop = 0;
function upDownScroll() {
	// var lastScrollTop = 0;
	var scrollBodyHeight = $('body').prop('scrollHeight');
	var winHeight = $(window).height();

	var st = $(window).scrollTop(),
		header = $('body header'),
		headerChild = header.find('.header-dafault'),
		footerGnb = $('.footer-gnb-wrap'),
		footerGnbChild = footerGnb.find('.footer-gnb'),
		footerFloating = $('.footer-gnb-floating');

	if (headerChild.is('.header-layout')) {
		if (st <= 50) {
			headerChild.removeClass('down');
		} else if (st >= scrollBodyHeight - winHeight - 10) {
			headerChild.addClass('down');
		} else {
			if (st > lastScrollTop) {
				// downscroll code
				headerChild.addClass('down');
				// scroll 이동시 sub dropdown menu 닫기
				if (header.find('#header-dropdown-menu').length != 0) {
					$('#header-dropdown-menu')
						.next('.pitem-menu__dropdown')
						.slideUp('fast');
				}
			} else {
				// upscroll code
				headerChild.removeClass('down');
			}
		}
	}
	// if (footerGnbChild.is('.fixed')) {
	// 	if (st <= 50) {
	// 	} else if (st >= scrollBodyHeight - 50) {
	// 		footerGnbChild.addClass('up');
	// 		footerFloating.addClass('up');
	// 	} else {
	// 		if (st > lastScrollTop) {
	// 			// downscroll code
	// 			footerGnbChild.removeClass('up');
	// 			footerFloating.removeClass('up');
	// 		} else {
	// 			// upscroll code
	// 			footerGnbChild.addClass('up');
	// 			footerFloating.addClass('up');
	// 		}
	// 	}
	// }
	if (st <= 150) {
		$('.button-scroll-top').hide();
	} else {
		$('.button-scroll-top').show();
	}
	if (
		footerGnbChild.is('.fixed') ||
		$('section').is('.pitem-header-btns-wrap')
	) {
		if (footerGnbChild.is('.fixed')) {
			if (st <= 50) {
			} else if (st >= scrollBodyHeight - 50) {
				footerGnbChild.removeClass('up');
				footerFloating.removeClass('up');
			} else {
				if (st > lastScrollTop) {
					// downscroll code
					footerGnbChild.addClass('up');
					footerFloating.addClass('up');
				} else {
					// upscroll code
					footerGnbChild.removeClass('up');
					footerFloating.removeClass('up');
				}
			}
		}

		if (st <= 150) {
			$('#button-footer-float').addClass('hide-scroll-top');
			$('#backbutton-footer-float').addClass('hide-scroll-top');
			$('#float-menu').addClass('hide-scroll-top');
		} else {
			$('#button-footer-float').removeClass('hide-scroll-top');
			$('#backbutton-footer-float').removeClass('hide-scroll-top');
			$('#float-menu').removeClass('hide-scroll-top');
		}
	}
	lastScrollTop = st;
}
$(document).ready(function () {
	upDownScroll();
});
$(window).on('scroll', function (e) {
	upDownScroll();
});
// 모바일 버튼 플로팅 메뉴
$(document)
	.on('click', '#button-footer-float', function () {
		$(this).toggleClass('is-active');
		$(this).toggleClass('modal-button');
	})
	.on('click', '#button-footer-float.is-active', function () {
		$('html').removeClass('is-clipped');
		var thisTarget = $(this).data('target');
		$('#' + thisTarget).removeClass('is-active');
	})
	.on('click', '#float-menu .modal-background', function () {
		$('html').removeClass('is-clipped');
		$(this).closest('.modal').removeClass('is-active');
		$('#button-footer-float')
			.removeClass('is-active')
			.addClass('modal-button');
	});

// document.addEventListener('DOMContentLoaded', function () {
// 	setTimeout(function () {
// 		upDownScroll();
// 	}, 100);
// });

// mobile main header dropdown
$(document).on('click', '#header-dropdown-menu', function () {
	$(this).toggleClass('is-active');
	// $(this).next('.pitem-menu__dropdown').slideToggle('fast');
	$('#pitem-menu__dropdown_re').addClass('is-active'); //브랜드 선택시 메뉴노출
});
$(document).on(
	'click',
	'#pitem-menu__dropdown_re .modal-background',
	function () {
		$('#header-dropdown-menu').removeClass('is-active');
		$('#pitem-menu__dropdown_re').removeClass('is-active');
	}
);

// footer top 위치 이동 스크롤
$(document).on('click', '.button-scroll-top', function () {
	$('body, html').animate({ scrollTop: 0 });
});

// S : a 태그의 href 를 이용해 현재 페이지 스크롤 처리 : 20200615
$(document).on(
	'click',
	'.tab-block-item > a[href^="#"], .box-block-item > a[href^="#"]',
	function (e) {
		var id = $(this).attr('href');
		var $id = $(id);
		if ($id.length === 0) {
			return;
		}
		// prevent standard hash navigation (avoid blinking in IE)
		e.preventDefault();
		// top position relative to the document
		var pos = $id.offset().top;
		// animated top scrolling
		$('body, html').animate({ scrollTop: pos });
	}
);
// E : a 태그의 href 를 이용해 현재 페이지 스크롤 처리

// S : Dropdowns
// Functions
function getAll(selector) {
	return Array.prototype.slice.call(document.querySelectorAll(selector), 0);
}

var $dropdowns = getAll('.dropdown:not(.is-hoverable)');
$(document)
	.on('click', function (e) {
		if (!$(e.target).closest('.dropdown').length > 0) {
			$('.dropdown.is-active').removeClass('is-active');
		}
	})
	.on('mouseenter focus', '.dropdown-trigger .button', function (e) {
		$('.dropdown').removeClass('is-hover');
		$(this).closest('.dropdown').addClass('is-hover');
	})
	.on('mouseleave', '.dropdown-trigger .button', function (e) {
		$(this).closest('.dropdown').removeClass('is-hover');
	})
	.on('click', '.dropdown-trigger .button', function (e) {
		e.preventDefault();
		$('.dropdown:not(.is-hover)').removeClass('is-active');
		if ($(this).closest('.dropdown').hasClass('is-active')) {
			$(this).closest('.dropdown').removeClass('is-active');
		} else {
			$(this).closest('.dropdown').addClass('is-active');
		}
	})
	.on('click', '.dropdown-item', function (e) {
		// 드롭다운 관련 추가 : 드롭다운 dropdown 아이템 아이콘 관련 이벤트 처리
		if ($(this).hasClass('is-icon')) {
			$(this).addClass('is-active').siblings().removeClass('is-active');
			$(this)
				.closest('.dropdown')
				.find('[aria-controls="dropdown-menu"]')
				.children('span')
				.eq(0)
				.empty()
				.append($(this).children().clone());
			$(this)
				.closest('.dropdown')
				.find('[aria-controls="dropdown-menu"]')
				.children('span')
				.eq(0)
				.append($(this).text());
		} else {
			$(this).addClass('is-active').siblings().removeClass('is-active');
			$(this)
				.closest('.dropdown')
				.find('[aria-controls="dropdown-menu"]')
				.children('span')
				.eq(0)
				.text($(this).text());
		}

		var dropdownWidth = $(this).outerWidth();
		if ($('.header-search__dropdown').length > 0) {
			$(this)
				.closest('.header-search__dropdown')
				.css('min-width', dropdownWidth)
				.find('.dropdown')
				.css('width', 'auto');
		}
		$('.dropdown.is-active').removeClass('is-active');
	});

function closeDropdowns() {
	$dropdowns.forEach(function ($el) {
		$el.classList.remove('is-active');
	});
}
// E : Dropdowns
// Modals 수정 : 200722
// Modal Open
// ios 포함 스크롤 불가능하게
function mobileSrollDisable() {
	$('html').addClass('is-clipped');
	document.addEventListener(
		'scroll touchmove mousewheel',
		(e) => {
			e.preventDefault();
		},
		{ passive: true } // console 에서 passive 출력 방지
	);
	// $('body').on('scroll touchmove mousewheel', function (e) {
	// 	e.preventDefault();
	// // });
	// }, {
	// 	passive: true
	// });
}
// ios 포함 스크롤 가능
function mobileSrollAble() {
	$('html').removeClass('is-clipped');
	$('body').off('scroll touchmove mousewheel');
}
// 모바일 푸터 앱바 보이기
function footerNavbarShow() {
	$('.footer-gnb.fixed').show();
	$('#button-footer-float').show();
	$('#button-scroll-top').show();
}
// 모바일 푸터 앱바 숨기기
function footerNavbarHide() {
	$('.footer-gnb.fixed').hide();
	$('#button-footer-float').hide();
	$('#button-scroll-top').hide();
}
$(document).on('click', '.modal-button', function (e) {
	e.preventDefault();
	if ($(this).attr('id') === 'button-footer-float') {
		// console.log($(this).attr('id'))
	} else {
		footerNavbarHide();
	}
	var target = '#' + $(this).data('target');
	mobileSrollDisable();
	$(target).addClass('is-active');
});
// Modal Close
$(document).on(
	'click',
	'.modal-background, .modal-close, .modal-card-head .delete, .modal .button-modal-close',
	function (e) {
		// closeModals();
		$(this).closest('.modal').removeClass('is-active');
		if ($('.modal.is-active').length < 1) {
			mobileSrollAble();
		}
		footerNavbarShow();
	}
);

function closeModals() {
	mobileSrollAble();
	$(document).find('.modal').removeClass('is-active');
}
$(document).on('keydown', function (event) {
	var e = event || window.event;
	if (e.keyCode === 27) {
		closeModals();
		// closeDropdowns();
		$('.dropdown.is-active').removeClass('is-active');
	}
});
document.addEventListener('keydown', function (event) {
	var e = event || window.event;
	if (e.keyCode === 27) {
		closeModals();
		closeDropdowns();
	}
});

// 탭 블록
$(document).on('click', '.tab-block-item', function (e) {
	$(this).addClass('is-active').siblings().removeClass('is-active');
});
// 탭 블록 2
$(document).on('click', '.box-block-item', function (e) {
	$(this).addClass('is-active').siblings().removeClass('is-active');
});

// S : 상단 헤더 통합검색 바
$(document)
	.on('click', function (e) {
		if (!$(e.target).closest('.header-search-wrap').length > 0) {
			$('.header-search-content').hide();
		}
		if ($(e.target).closest('.dropdown-wrap').length > 0) {
			$('.header-search-content').hide();
		}
	})
	.on('click', '.header-search__input', function (e) {
		e.preventDefault();
		$('#headerSearchContent').show();
		$('html, body').css({ overflow: 'hidden', height: '100%' });
		// $('#headerSearchContent').on('scroll touchmove mousewheel', function (event) {
		// 	event.preventDefault();
		// 	event.stopPropagation();
		// });
	})
	.on('click', '.button-search-content__del', function (e) {
		e.stopPropagation();
		$(this).closest('li').remove();
		$(this)
			.closest('.tabpanel')
			.find('.header-search-content__body li')
			.remove();
	})

	.on('click', '.button-search-all__del', function (e) {
		e.preventDefault();
		$(this)
			.closest('.tabpanel')
			.find('.header-search-content__body li')
			.remove();
		if (
			$(this).closest('.tabpanel').find('.header-search-content__body li')
				.length <= 0
		) {
			$('.no-search__info').addClass('is-active');
		} else {
			$('.no-search__info').removeClass('is-active');
		}
	})
	.on('click', '.button-search-content__close', function (e) {
		e.preventDefault();
		$(this).closest('.header-search-content').hide();
		$('html, body').css({ overflow: 'auto', height: '100%' });
		$('#headerSearchContent').off('scroll touchmove mousewheel');
	})
	.on('keydown keyup', '.header-search-wrap form', function (event) {
		var e = event || window.event;
		if (e.keyCode === 13) {
			$('.dropdown.is-active').removeClass('is-active');
		}
	})
	.on('keyup', '.header-search__input', function (e) {
		if ($(this).val() != '') {
			$('#headerSearchContent').hide();
			$('#headerSearchKeyword').show();
		} else {
			$('#headerSearchContent').show();
			$('#headerSearchKeyword').hide();
		}
	});
// E : 상단 헤더 통합검색 바

// 빈값 체크
var isEmpty = function (value) {
	if (
		value == '' ||
		value == null ||
		value == undefined ||
		(value != null &&
			typeof value == 'object' &&
			!Object.keys(value).length)
	) {
		return true;
	} else {
		return false;
	}
};

// 제목 글자수 카운트 : 200908
$(document).on('keydown, blur', '[maxlength]', function () {
	var content = $(this).val();
	// var thisMaxL = Number($(this).attr('maxlength')) - 1;
	var thisMaxL = Number($(this).attr('maxlength'));

	if (content.length > thisMaxL) {
		alert('최대 ' + thisMaxL + '자까지 입력 가능합니다.');
		$(this).val(content.substring(0, thisMaxL));
		return;
	}
});

// 푸터 패밀리 링크 열기
$(document)
	.on('click', '.button-family-site', function () {
		$(this).parent().toggleClass('is-active');
	})
	.on('click', '.footer-family-site-list a', function () {
		$('.footer-family-site').removeClass('is-active');
	});

// 푸터 패밀리 링크 이동 : 200909
// 앱 에서의 이중 호출건으로 주석처리 : 201112
/*
function fnLoadLink(url) {
	var ua = navigator.userAgent;
	if (ua !== undefined ) {
		var iOS = /iPad|iPhone|iPod/.test(ua);
		if (iOS) {
			window.location = url;
		} else {
			window.open(url, '_blank');
		}
	}
}
$(document).on('change', '.footer-bottom-selectbox select', function () {
	var thisTarget = $(this).find('option:selected').val();
	if (!isEmpty(thisTarget)) {
		fnLoadLink(thisTarget);
	}
});
*/

// 헤더 검색 창 클릭 및 푸터 검색 버튼 클릭
// 검색창 내용 지우기
function searchClearInput() {
	var el = document.getElementsByClassName('header-search__input');
	for (var i = 0; i < el.length; i++) {
		el[i].value = '';
	}
}
$(document)
	// .on('click', '.button-open-modal-search, .pitem-middle__search input[type=search]', function () {
	// 제품상세 헤더 검색 버튼 클릭 이벤트 추가 : 201112
	.on(
		'click',
		'.pitem-middle__search input[type=search], .button-header-search',
		function () {
			var $target = $(
				'#modal-search-pop .header-search__input-wrap > input'
			);
			$('html').addClass('is-clipped');
			$('#modal-search-pop').addClass('is-active');
			// $target.trigger('touchstart');
			$target.focus();
			// $target.keypress();
			setTimeout(function (e) {
				$target.click();
				$target.focus();
			}, 50);
			// setTimeout(function (e) {
			// 	$target.focus();
			// }, 100);
		}
	)
	// .on('touchstart', '#modal-search-pop .header-search__input-wrap > input', function () {
	// 	$(this).focus();
	// 	setTimeout(function (e) {
	// 		$(this).click();
	// 	}, 50);
	// })
	.on('click', '#modal-search-pop .modal-card-head .delete', function () {
		$('html').removeClass('is-clipped');
		$('html, body').css({ overflow: '', height: '' });
	});

// fixed 체크 : 200915
var $scrollFixed = $('.scroll-fixed-box');
function check_scroll_fixed(_wrapper) {
	var wrapper_top = $(_wrapper).offset().top;
	var scroll_top = $(window).scrollTop();
	var topY;
	var padY;
	var boxH = $(_wrapper).children().outerHeight();
	if ($('#header-closed').length) {
		if ($('.header-dafault.fixed').length) {
			topY = 55;
			if ($('#container').length) {
				if (scroll_top > wrapper_top - 50) {
					$(_wrapper).addClass('fixed');
					$(_wrapper)
						.children()
						.css({ position: 'fixed', top: topY + 'px' });
					$(_wrapper).css({ 'padding-top': '55px' });
				} else {
					$(_wrapper).children().css({ transform: 'none' });
					$(_wrapper)
						.children()
						.css({ position: 'relative', top: 'initial' });
					$(_wrapper).removeClass('fixed');
					$(_wrapper).css({ 'padding-top': '' });
				}
			}
		}
	} else {
		if ($('.header-dafault.fixed').length) {
			if ($('.header-dafault.fixed').hasClass('down')) {
				topY = 50;
				$(_wrapper).children().css({ transform: 'none' });
			} else {
				$(_wrapper).children().css({ transform: 'none' });
				if ($('.pitem-top-wrap').length) {
					topY = 50;
					$(_wrapper)
						.children()
						.css({ transform: 'translateY(55px)' });
				} else {
					topY = 50;
					// topY = 105;
				}
				// if (boxH < 50) {
				// 	topY = 60 + boxH;
				// } else {
				// 	topY = 55 + boxH;
				// }
			}

			if ($('#container').length) {
				padY = topY + boxH;
				if (scroll_top > wrapper_top - 50) {
					$(_wrapper).addClass('fixed');
					$(_wrapper)
						.children()
						.css({ position: 'fixed', top: topY + 'px' });
					$(_wrapper).css({ 'padding-top': padY + 'px' });
				} else {
					$(_wrapper).children().css({ transform: 'none' });
					$(_wrapper)
						.children()
						.css({ position: 'relative', top: 'initial' });
					$(_wrapper).removeClass('fixed');
					$(_wrapper).css({ 'padding-top': '' });
				}
			}
		}
	}
}
$(window).on('scroll', function () {
	if ($('.scroll-fixed-box').length) {
		check_scroll_fixed('.scroll-fixed-box');
	}
});
// E : fixed 스크롤 체크

// 기획전 상세 등 스크롤 영역 체크해서 active 표시 해 주기 : 200915
function add_scroll_area($el, index) {
	var tempW = 0;
	$el.each(function () {
		if ($(this).index() < index) {
			tempW += $(this).width() + 10;
		}
	});
	return tempW;
}
function scroll_area_check() {
	var scroll_top = $(window).scrollTop();
	if ($('[role="scrollcheck"]').length) {
		$('[role="scrollcheck"]').each(function () {
			var thisTarget = $(this).attr('href');
			if (
				thisTarget.startsWith('#') &&
				$(thisTarget) != undefined &&
				$(thisTarget).offset() != undefined
			) {
				var itemTop = $(thisTarget).offset().top - 50;
				if (scroll_top > itemTop) {
					$('[role="scrollcheck"]').parent().removeClass('is-active');
					$('[href="' + thisTarget + '"]')
						.parent()
						.addClass('is-active');
				}
			}
		});
	}
}
$(function () {
	if ($('[role="scrollcheck"]').length) {
		scroll_area_check();
	}
});
$(window).on('scroll resize', function () {
	if ($('[role="scrollcheck"]').length) {
		scroll_area_check();
	}
});

// http://jsfiddle.net/04fLy8t4/ : resize 이벤트 정의 함수
var resizeEventsTrigger = (function () {
	function triggerResizeStart($el) {
		$el.trigger('resizestart');
		isStart = !isStart;
	}

	function triggerResizeEnd($el) {
		clearTimeout(timeoutId);
		timeoutId = setTimeout(function () {
			$el.trigger('resizeend');
			isStart = !isStart;
		}, delay);
	}

	var isStart = true;
	var delay = 200;
	var timeoutId;

	return function ($el) {
		isStart ? triggerResizeStart($el) : triggerResizeEnd($el);
	};
})();

$(document).ready(function () {
	// iOS 용 모달 창에 focus 발생시 커서 올바르게 작동시키기
	var ua = navigator.userAgent;
	if (ua !== undefined) {
		var iOS = /iPad|iPhone|iPod/.test(ua);
		if (iOS) {
			$(document).on('focus', 'input', function () {
				window.scrollBy(0, 2);
				window.scrollBy(0, -2);
			});
		}
	}

	// 쿠폰이벤트, 쿠폰팝업에서 자릿수 클경우 크기 수정
	if ($('.coupon-card-top').length) {
		$('.coupon-card-top').each(function () {
			var thisText = $(this).find('.num-off__amount').text();
			// console.log(thisText.length);
			var strFontSize;
			if (thisText.length > 12) {
				strFontSize = '42px';
			} else if (thisText.length > 10 && thisText.length <= 12) {
				strFontSize = '46px';
			} else if (thisText.length > 8 && thisText.length <= 10) {
				strFontSize = '54px';
			} else if (thisText.length > 6 && thisText.length <= 8) {
				strFontSize = '63px';
			} else if (thisText.length > 4 && thisText.length <= 6) {
				strFontSize = '80px';
			} else if (thisText.length > 3 && thisText.length <= 4) {
				strFontSize = '95px';
			} else {
				strFontSize = '110px';
			}
			$(this).find('.num-off__amount').css({ 'font-size': strFontSize });
		});
	}
});
