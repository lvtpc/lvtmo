if ($('.event_0101').length) {
	$(document).on('click','.tab a',function(event){
		var targetHash = $(this).attr('href');
		event.preventDefault();
		$("html,body").animate({
			scrollTop : $(targetHash).offset().top - 215
        },300)
    });
    $(".accordion a").on("click", function() {
		if ($(".accordion").hasClass("open")) { 
			$(".accordion").removeClass("open");
		}
		else {
			$(".accordion").addClass("open");
		}
	});
}

if ($('.event_0401').length) {
	$(".accordion a").on("click", function() {
		if ($(".accordion").hasClass("open")) { 
			$(".accordion").removeClass("open");
		}
		else {
			$(".accordion").addClass("open");
		}
	});
}

$(document).ready(function() {
	$(".live_tab button").click(function(){
		var tab_id=$(this).attr("data-tab");
		$(".live_tab button").removeClass("current");
		$(".tab_content > a").removeClass("current");
		$(this).addClass("current");
		$("#"+tab_id).addClass("current")
	})
});

// 이리온 오픈 이벤트용 카운트 다운 : 201125
$(document).ready(function () {
	if ($('#countdown-time-module').length) {
		CountDownEventTimer('countdown-time-module');
	}
});
function CountDownEventTimer(id) {
	var now = new Date();
	var curY = now.getFullYear();
	var curM = now.getMonth() + 1;
	var curD = now.getDate();
	var end = new Date(curY, curM, curD, 24, 0, 0, 0);
	// console.log("end  :", now);
	var _second = 1000;
	var _minute = _second * 60;
	var _hour = _minute * 60;
	var _day = _hour * 24;
	var timer;

	function showRemaining() {
		var now = new Date();
		var distance = end - now;
		if (distance < 0) {
			clearInterval(timer);
			document.getElementById(id).innerHTML = '00 : 00 : 00';
			return;
		}
		var hours = '0' + Math.floor((distance % _day) / _hour);
		var minutes = '0' + Math.floor((distance % _hour) / _minute);
		var seconds = '0' + Math.floor((distance % _minute) / _second);
		document.getElementById(id).innerHTML = '<span>' + hours.slice(-2) + ' : ' + minutes.slice(-2) + ' : ' + seconds.slice(-2) + '</span>';
	}
	timer = setInterval(showRemaining, 1000);
}
/*
function CountDownEventTimer(dt, id) {
	var arrCount = dt.split(':');
	var _second = parseInt(arrCount[2]);
	var _minute = parseInt(arrCount[1]);
	var _hour = parseInt(arrCount[0]);
	var timer;
	var timeAmount = _hour * 3600 + _minute * 60 + _second;

	function showRemaining() {
		var hours, minutes, seconds;
		if (timeAmount <= 0) {
			clearInterval(timer);
			document.getElementById(id).innerHTML = '00 : 00 : 00';
			return;
		}
		if (timeAmount >= 3600) {
			hours = parseInt(timeAmount / 3600);
		} else {
			hours = 0;
		}
		var timeRemain1 = timeAmount - (hours * 3600);
		if (timeRemain1 >= 60) { 
			minutes = parseInt(timeRemain1 / 60);
		} else {
			minutes = 0;
		}
		seconds = timeRemain1 - (minutes * 60);
		hours = '0' + hours;
		minutes = '0' + minutes;
		seconds = '0' + seconds;
		document.getElementById(id).innerHTML = '<span>' + hours.slice(-2) + ' : ' + minutes.slice(-2) + ' : ' + seconds.slice(-2) + '</span>';
		timeAmount--;
	}
	timer = setInterval(showRemaining, 1000);
}
*/

// 그랜드 오픈 이벤트용 스크립트
// S : 헤더 스크롤 픽스 가로 스크롤 이동 : 200917
function scroll_area_move(el) {
	var scroll_top = $(window).scrollTop();
	var wrapper_top = $(el).offset().top;
	var tempIndex;
	var tempW;
	var $obj = $(el).find('.box-block');
	var $objItem = $obj.find('.box-block-item');
	if ($obj.length) {
		// active 위치 표시
		$obj.find('a').each(function () {
			var thisTarget = $(this).attr('href');
			if (thisTarget.startsWith('#') && $(thisTarget) != undefined && $(thisTarget).offset() != undefined) {
				var itemTop = $(thisTarget).offset().top - 50;
				if (scroll_top > itemTop) {
					// 위치 지정
					tempIndex = $('[href="' + thisTarget + '"]')
						.parent()
						.index();
				}
			}
		});
		// active 위치 이동
		var outerWidth = $obj.find('.box-block-item-lists').outerWidth();
		if ($objItem.length) {
			tempW = add_scroll_area($objItem, tempIndex);
			if (tempW < 0) {
				tempW = 0;
			} else if (tempW > outerWidth) {
				tempW = outerWidth;
			}
			$obj.scrollLeft(tempW);
			// $obj.stop().animate({ scrollLeft: tempW }, 100, 'swing');
		}
		if (scroll_top > wrapper_top) {
			$(el).addClass('is-fixed');
			$(el).children().css({ position: 'fixed', top: '50px' });
			$(el).css({ 'padding-top': '140px' });
		} else {
			$(el).children().css({ position: 'relative', top: 'initial' });
			$(el).removeClass('is-fixed');
			$(el).css({ 'padding-top': '' });
		}
	}
}
$(function () {
	if ($('.event_0401').length) {
		var scrollFixedBox = '.scroll-fixed-box';
		if ($(scrollFixedBox).length) {
			scroll_area_move(scrollFixedBox);
	
			$(window).on('scroll resize', function () {
				scroll_area_move(scrollFixedBox);
			});
		}
	}
});
// E : 헤더 스크롤 픽스 가로 스크롤 이동

$(function () {
	if ($('.event_0301').length) {
		var scrollFixedBox = '.scroll-fixed-box';
		if ($(scrollFixedBox).length) {
			scroll_area_move(scrollFixedBox);
	
			$(window).on('scroll resize', function () {
				scroll_area_move(scrollFixedBox);
			});
		}
	}
});